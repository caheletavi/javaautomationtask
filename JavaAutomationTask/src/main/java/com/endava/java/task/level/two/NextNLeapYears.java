package com.endava.java.task.level.two;

import java.time.LocalDate;

//Write a program that outputs the next k leap years
public class NextNLeapYears {

    private static void getNextNLeapYears(int nrOfYears) {
        int count = 0;
        int thisYear = LocalDate.now().getYear();
        while (count != nrOfYears) {
            if (isLeapYear(thisYear)) {
                System.out.println(thisYear);
                count++;
            }
            thisYear++;
        }
    }

    private static boolean isLeapYear(int thisYear) {
        return (thisYear % 4 == 0 && thisYear % 100 != 0) || thisYear % 400 == 0;
    }

    public static void runNextNLeapYears(int nrOfYears) {
        getNextNLeapYears(nrOfYears);
    }
}
