package com.endava.java.task.level.one;

//Given a string of characters, determine if it is a palindrome or not
public class Palindrome {
    private static boolean isPalindrom(String value) {
        StringBuffer reverseValue = new StringBuffer(value).reverse();
        return reverseValue.toString().equals(value);
    }

    public static void runPalindrome(String value) {
        System.out.println(isPalindrom(value) ? "is palindrome" : "is not palindrome");
    }
}
