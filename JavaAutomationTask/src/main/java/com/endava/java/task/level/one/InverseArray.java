package com.endava.java.task.level.one;
//Given an array of floating point numbers, inverse the elements of the array

import org.apache.commons.lang3.ArrayUtils;

public class InverseArray {
    private static int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30};

    private static void inverseUsingLibraries(int[] array) {
        if (array == null || array.length == 0)
            return;
        ArrayUtils.reverse(array);
    }

    private static void inverseUsingForLoop(int[] array) {
        if (array == null || array.length == 0)
            return;
        for (int i = 0; i < array.length / 2; i++) {
            int temp = array[i];
            array[i] = array[array.length - i - 1];
            array[array.length - i - 1] = temp;
        }

    }

    private static void inverseUsingWhileLoop(int[] array) {
        int leftIndex = 0, rightIndex = array.length - 1;
        if (array.length == 0)
            return;
        while (leftIndex < rightIndex) {
            int temp = array[leftIndex];
            array[leftIndex] = array[rightIndex];
            array[rightIndex] = temp;
            leftIndex++;
            rightIndex--;
        }
    }

    private static void printArray(int[] array) {
        if (array == null || array.length == 0)
            return;
        for (int v : array)
            System.out.print(v + " ");
        System.out.println("\n");
    }

    public static void runInverseArray() {
        printArray(array);
        inverseUsingLibraries(array);
        printArray(array);
        inverseUsingForLoop(array);
        printArray(array);
    }

    public static void main(String[] args) {
        long start = System.nanoTime();
        inverseUsingForLoop(array);
        System.out.println(System.nanoTime() - start + "\tnanos FOR");

        long start1 = System.nanoTime();
        inverseUsingWhileLoop(array);
        System.out.println(System.nanoTime() - start1 + "\tnanos WHILE");

        long start2 = System.nanoTime();
        inverseUsingLibraries(array);
        System.out.println(System.nanoTime() - start2 + "\tnanos LIBRARIES");
    }
}


