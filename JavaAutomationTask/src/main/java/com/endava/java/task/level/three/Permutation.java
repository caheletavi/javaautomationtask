package com.endava.java.task.level.three;

import java.util.Arrays;

//Given an integer n, generate permutations of n elements - note that the amount of permutations of n elements is n!
public class Permutation {
    private static int[] array = {1, 2, 3, 4};

    private static void swap(int[] array, int firstElement, int secondElement) {
        int temp = array[firstElement];
        array[firstElement] = array[secondElement];
        array[secondElement] = temp;
    }

    private static void permute(int[] array, int startIndex, int endIndex) {
        if (startIndex == endIndex) {
            System.out.println(Arrays.toString(array));
        } else {
            for (int i = startIndex; i <= endIndex; i++) {
                swap(array, startIndex, i);
                permute(array, startIndex + 1, endIndex);
                swap(array, startIndex, i);
            }
        }
    }

    public static void main(String[] args) {
        permute(array, 0, array.length - 1);
    }
}
