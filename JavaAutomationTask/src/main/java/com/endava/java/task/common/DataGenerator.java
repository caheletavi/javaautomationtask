package com.endava.java.task.common;

import java.util.Random;

public class DataGenerator {

    public static int[] generateRandomArray(int arraySize) {
        int[] result = new int[arraySize];
        for (int i = 0; i < arraySize; i++)
            result[i] = new Random().nextInt(9999) + 1;
        return result;
    }

    public static int[][] generateRandomMatrix(int nrOfLines, int nrOfColumns) {
        int[][] result = new int[nrOfLines][nrOfColumns];
        for (int i = 0; i < nrOfLines; i++)
            for (int j = 0; j < nrOfColumns; j++)
                result[i][j] = new Random().nextInt(9999) + 1;
        return result;
    }

//    private static void printArray(int[] array) {
//        for (int element : array)
//            System.out.print(element + " ");
//        System.out.println();
//    }
//
//    private static void printMatrix(int[][] matrix) {
//        for (int i = 0; i < matrix.length; i++) {
//            for (int j = 0; j < matrix[i].length; j++)
//                System.out.print(matrix[i][j] + " \t");
//            System.out.println();
//        }
//    }
//
//    public static void main(String[] args) {
//        int[] array = generateRandomArray(30);
//        printArray(array);
//        System.out.println(array.length);
//
//        int[][] matrix = generateRandomMatrix(4,5);
//        printMatrix(matrix);
//
//    }
}
