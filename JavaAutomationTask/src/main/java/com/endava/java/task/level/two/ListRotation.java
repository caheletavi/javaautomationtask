package com.endava.java.task.level.two;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ListRotation {
    private static List<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));

    private static void rotateList(List<Integer> list, int nrOfElements) {
        Collections.rotate(list, -nrOfElements);
    }

    public static void main(String[] args) {
        System.out.println(list);
        System.out.println(list.size());
        rotateList(list, 13);
        System.out.println(list);
    }
}
