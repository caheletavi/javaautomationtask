package com.endava.java.task.level.one;

import com.endava.java.task.common.DataGenerator;

public class BubbleSort {
        private static int[] array = DataGenerator.generateRandomArray(300);
    static int count = 0;
    static int count1 = 0;

    private static void useImprovedBubbleSort(int[] array) {
        boolean flag;
        int maxIndex = array.length - 1;
        for (int i = 0; i < maxIndex; i++) {
            flag = false;
            for (int j = i + 1; j < maxIndex - i; j++) {
                if (array[i] > array[j]) {
                    int temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                    flag = true;
                    count++;
                }
            }
            if (!flag)
                break;
        }
    }

    private static void useClassicBubbleSort(int[] array) {
        for (int i = 0; i < array.length - 1; i++)
            for (int j = i + 1; j < array.length; j++)
                if (array[i] > array[j]) {
                    int temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                    count1++;
                }
    }

    private static void printArray(int[] array) {
        if (array == null || array.length == 0)
            return;
        for (int v : array)
            System.out.print(v + " ");
        System.out.println("\n");
    }

    public static void runBubbleSort() {
        printArray(array);
        useImprovedBubbleSort(array);
        printArray(array);
        System.out.println("Count: " + count);
//        useClassicBubbleSort(array);
//        printArray(array);
//        System.out.println("Count: " + count1);
    }

    public static void main(String[] args) {
        runBubbleSort();
    }
}
