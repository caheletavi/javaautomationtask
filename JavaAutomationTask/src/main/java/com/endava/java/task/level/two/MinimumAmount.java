package com.endava.java.task.level.two;

//Given a list of coins with different values (1, 3, 10, 25, 50), determine the minimum amount of coins needed to sum up a certain amount of money
public class MinimumAmount {
    private static int[] coins = {1, 3, 10, 25, 50};

    private static int calculateTheMinimumAmount(int[] coins, int nrOfCoins, int amount) {
        int[] table = new int[amount + 1];
        table[0] = 0;
        setValuesToMax(table);
        for (int i = 1; i <= amount; i++) {
            for (int coinIndex = 0; coinIndex < nrOfCoins; coinIndex++)
                if (coins[coinIndex] <= i) {
                    int substractionResult = table[i - coins[coinIndex]];
                    if (substractionResult != Integer.MAX_VALUE & substractionResult + 1 < table[i])
                        table[i] = substractionResult + 1;
                }
        }
        return table[amount];
    }

    private static void setValuesToMax(int[] table) {
        for (int i = 1; i < table.length; i++)
            table[i] = Integer.MAX_VALUE;
    }

    public static void main(String[] args) {
        int amount = 131;
        System.out.println("Minimum coins required is " + calculateTheMinimumAmount(coins, coins.length, amount));
    }
}
