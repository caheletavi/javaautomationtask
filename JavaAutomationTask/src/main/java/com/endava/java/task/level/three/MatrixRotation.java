package com.endava.java.task.level.three;

// Write a program that will rotate a two-dimensional array clockwise
public class MatrixRotation {
    private static int[][] matrix = {
            {1, 2, 3, 4},
            {5, 6, 7, 8},
            {9, 10, 11, 12}
    };

    private static int[][] rotateMatrixClockwise(int[][] matrix) {
        int[][] result = new int[matrix[0].length][matrix.length];
        for (int i = 0; i < matrix[0].length; i++)
            for (int j = 0; j < matrix.length; j++)
                result[i][j] = matrix[matrix.length - j - 1][i];
        return result;
    }

    private static int[][] rotateMatrixCounterclockwise(int[][] matrix) {
        int[][] result = new int[matrix[0].length][matrix.length];
        for (int i = 0; i < matrix[0].length; i++)
            for (int j = 0; j < matrix.length; j++)
                result[i][j] = matrix[j][matrix[j].length - i - 1];
        return result;
    }

    private static void printMatrix(int[][] matrix) {
        for (int[] matrix1 : matrix) {
            for (int element : matrix1)
                System.out.print(element + "\t");
            System.out.println();
        }
    }

    public static void main(String[] args) {
        printMatrix(matrix);
        System.out.println("----------------------------");
        int[][] result = rotateMatrixClockwise(matrix);
        printMatrix(result);
        System.out.println("----------------------------");
        printMatrix(rotateMatrixCounterclockwise(matrix));
    }
}