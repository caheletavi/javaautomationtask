package com.endava.java.task.level.one;

public class SumAndAmount {
    private static int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    private static int getSumOfOdd(int[] array) {
        int result = 0;
        if (array == null || array.length == 0)
            return 0;
        for (int elem : array)
            result += elem % 2 == 0 ? elem : 0;
        return result;
    }

    private static int getAmountOfEven(int[] array) {
        int count = 0;
        if (array == null || array.length == 0)
            return 0;
        for (int elem : array)
            count += elem % 2 != 0 ? 1 : 0;
        return count;
    }

    public static void runSumAndAmount(int[] array) {
        System.out.println("Sum of odd numbers: " + getSumOfOdd(array));
        System.out.println("Amount of even numbers: " + getAmountOfEven(array));
    }
}
