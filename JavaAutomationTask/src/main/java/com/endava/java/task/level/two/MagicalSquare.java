package com.endava.java.task.level.two;

//Write a program that will check if a two-dimensional arrays is magical square or not
public class MagicalSquare {
    private static int[][] magicSquareMatrix = {
            {9, 6, 3, 16},
            {4, 15, 10, 5},
            {14, 1, 8, 11},
            {7, 12, 13, 2}
    };

    private static boolean isMagicSquare(int[][] matrix) {
        int primeDiagonale = sumofThePrimeDiagonale(matrix);
        for (int i = 0; i < matrix.length; i++) {
            if (primeDiagonale != sumOfRow(i, matrix))
                return false;
        }
        for (int j = 0; j < matrix.length; j++) {
            if (primeDiagonale != sumOfCol(j, matrix))
                return false;
        }
        return true;
    }

    private static int sumOfCol(int colIndex, int[][] matrix) {
        int result = 0;
        for (int i = 0; i < matrix.length; i++)
            result += matrix[i][colIndex];
        return result;
    }

    private static int sumOfRow(int rowIndex, int[][] matrix) {
        int result = 0;
        for (int i = 0; i < matrix.length; i++)
            result += matrix[rowIndex][i];
        return result;
    }

    private static int sumofThePrimeDiagonale(int[][] matrix) {
        int result = 0;
        for (int i = 0; i < matrix[0].length; i++)
            result += matrix[i][i];
        return result;
    }

    public static void runMagicalSquare() {
        System.out.println(isMagicSquare(magicSquareMatrix) ? "Magic Square" : "Not a Magic Square");
    }
}
