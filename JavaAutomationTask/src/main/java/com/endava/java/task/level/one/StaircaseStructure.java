package com.endava.java.task.level.one;

public class StaircaseStructure {

    private static void drawStaircaseStructureToLeft(int requiredHeight) {
        for (int i = 0; i < requiredHeight; i++) {
            for (int j = 0; j <= i; j++)
                System.out.print("#");
            System.out.println();
        }
    }

    private static void drawStaircaseStructureToRight(int requiredHeight) {
        for (int i = 0; i < requiredHeight; i++) {
            for (int j = 0; j < requiredHeight; j++)
                System.out.print(i + j < requiredHeight - 1 ? " " : "#");
            System.out.println();
        }
    }

    public static void runStaircaseStructure(int requiredHeight) {
        drawStaircaseStructureToLeft(requiredHeight);
        System.out.println("------------------------------");
        drawStaircaseStructureToRight(requiredHeight);
    }
}
