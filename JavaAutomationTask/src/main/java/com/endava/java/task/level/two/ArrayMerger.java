package com.endava.java.task.level.two;

import java.util.Arrays;

//Given three ascendingly sorted arrays, merge the elements of these three arrays to obtain one ascendingly sorted array
public class ArrayMerger {
    private static int[] a = {1, 5, 5, 5, 6, 6, 6, 7, 7, 7, 8, 11, 23, 53, 88, 99};
    private static int[] b = {2, 6, 77};
    private static int[] c = {-10, 4, 7, 8};

    private static void printArray(int[] array) {
        System.out.print("[");
        for (int elem : array) {
            System.out.print(elem + " ");
        }
        System.out.println("]");
    }

    private static int[] newWayOfMerge(int[]... arrays) {
        return Arrays.stream(arrays).flatMapToInt(Arrays::stream).sorted().toArray();
    }

    private static int[] mergeThreeArrays(int[] firstArray, int[] secondArray, int[] thirdArray) {
        int[] result = new int[firstArray.length + secondArray.length + thirdArray.length];
        int firstIndex = 0, secondIndex = 0, thirdIndex = 0, resultIndex = 0;
        while (firstIndex < firstArray.length && secondIndex < secondArray.length && thirdIndex < thirdArray.length) {
            int minElement = Math.min(Math.min(firstArray[firstIndex], secondArray[secondIndex]), thirdArray[thirdIndex]);
            if (minElement == firstArray[firstIndex]) {
                result[resultIndex++] = minElement;
                firstIndex++;
            } else if (minElement == secondArray[secondIndex]) {
                result[resultIndex++] = minElement;
                secondIndex++;
            } else {
                result[resultIndex++] = minElement;
                thirdIndex++;
            }
        }
        while (firstIndex < firstArray.length && secondIndex < secondArray.length) {
            int minElement = Math.min(firstArray[firstIndex], secondArray[secondIndex]);
            if (minElement == firstArray[firstIndex]) {
                result[resultIndex++] = minElement;
                firstIndex++;
            } else {
                result[resultIndex++] = minElement;
                secondIndex++;
            }
        }
        while (firstIndex < firstArray.length && thirdIndex < thirdArray.length) {
            int minElement = Math.min(firstArray[firstIndex], thirdArray[thirdIndex]);
            if (minElement == firstArray[firstIndex]) {
                result[resultIndex++] = minElement;
                firstIndex++;
            } else {
                result[resultIndex++] = minElement;
                thirdIndex++;
            }
        }
        while (secondIndex < secondArray.length && thirdIndex < thirdArray.length) {
            int minElement = Math.min(secondArray[secondIndex], thirdArray[thirdIndex]);
            if (minElement == secondArray[secondIndex]) {
                result[resultIndex++] = minElement;
                secondIndex++;
            } else {
                result[resultIndex++] = minElement;
                thirdIndex++;
            }
        }
        while (firstIndex < firstArray.length)
            result[resultIndex++] = firstArray[firstIndex++];
        while (secondIndex < secondArray.length)
            result[resultIndex++] = secondArray[secondIndex++];
        while (thirdIndex < thirdArray.length)
            result[resultIndex++] = thirdArray[thirdIndex++];
        return result;
    }

    public static void main(String[] args) {
        printArray(a);
        printArray(b);
        printArray(c);
        long start = System.nanoTime();
//        printArray(mergeThreeArrays(a, b, c));
        printArray(newWayOfMerge(a, b, c));
        System.out.println("--------------------------------------");
        System.out.println(System.nanoTime() - start);
    }
}
