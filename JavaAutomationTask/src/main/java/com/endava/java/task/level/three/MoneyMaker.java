package com.endava.java.task.level.three;

//if you start with 1$ and, with each move, you can either double your money or add another 1$, what is the smallest number of moves you have to make to get exactly n$
public class MoneyMaker {

    private static int getNrOfMovesToMake(int dollars) {
        int count = 0;
        while (dollars != 1) {
            if (dollars % 2 == 0) {
                System.out.print(dollars + "/2 -> ");
                dollars /= 2;
                count++;
            } else {
                System.out.print(dollars + "-1 -> ");
                dollars--;
                count++;
            }
        }
        return count;
    }

    public static void main(String[] args) {
        System.out.println(21);
        System.out.println("\n" + getNrOfMovesToMake(21));
    }
}
