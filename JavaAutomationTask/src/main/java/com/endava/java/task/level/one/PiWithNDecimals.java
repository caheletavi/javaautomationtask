package com.endava.java.task.level.one;

import java.math.BigDecimal;

//Given an integer n, determine the number pi with an error less than n decimal places
public class PiWithNDecimals {

    private static BigDecimal getPiValueWithPrecisionOf(int precision) {
        return new BigDecimal(Math.PI).setScale(precision, BigDecimal.ROUND_HALF_UP);
    }

    public static void runPiWithNDecimals(int precision) {
        System.out.println(getPiValueWithPrecisionOf(precision));
    }
}