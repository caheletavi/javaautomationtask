package com.endava.java.task.level.two;

import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.IntStream;

public class MatrixSorter {

    private static int[][] matrix = {
            {21, 10, 0},
            {1, 3, 7},
            {8, -3, -10}
    };

    private static void sortUsingLibraries(int[][] matrix) {
        Arrays.sort(matrix, Comparator.comparingInt(oneLine -> IntStream.of(oneLine).sum()));
    }

    private static void swapLines(int firstLine, int secondLine, int[][] matrix) {
        for (int i = 0; i < matrix[firstLine].length; i++) {
            int temp = matrix[firstLine][i];
            matrix[firstLine][i] = matrix[secondLine][i];
            matrix[secondLine][i] = temp;
        }
    }

    private static int getLineSum(int lineIndex, int[][] matrix) {
        int sum = 0;
        for (int i = 0; i < matrix[lineIndex].length; i++)
            sum += matrix[lineIndex][i];
        return sum;
    }

    private static void sortUsingForLoop(int[][] matrix) {
        boolean flag;
        int maxIndex = matrix.length;
        for (int i = 0; i < maxIndex; i++) {
            flag = false;
            for (int j = i + 1; j < maxIndex - i; j++) {
                if (getLineSum(i, matrix) > getLineSum(j, matrix)) {
                    swapLines(i, j, matrix);
                    flag = true;
                }
            }
            if (!flag)
                break;
        }
    }

    private static void printMatrix(int[][] matrix) {
        for (int[] matrix1 : matrix) {
            for (int i : matrix1)
                System.out.print(i + "\t");
            System.out.println();
        }
    }

    public static void main(String[] args) {
        printMatrix(matrix);
        long start = System.nanoTime();
        System.out.println("-------------- SOME MAGIC -------------");
        sortUsingLibraries(matrix);
//        sortUsingForLoop(matrix);
        System.out.println(System.nanoTime() - start);
        printMatrix(matrix);
    }
}
