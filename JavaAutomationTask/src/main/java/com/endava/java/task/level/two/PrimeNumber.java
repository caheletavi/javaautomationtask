package com.endava.java.task.level.two;

//Given an integer n, determine all prime numbers smaller than n
public class PrimeNumber {

    private static void getSmallerPrimeNrThan(int number) {
        for (int i = 1; i < number; i++)
            if (isPrime(i))
                System.out.print(i + ", ");
    }

    private static boolean isPrime(int number) {
        if ((number > 2 && number % 2 == 0) || number == 1) {
            return false;
        }
        for (int i = 3; i <= (int) Math.sqrt(number); i += 2) {
            if (number % i == 0)
                return false;
        }
        return true;
    }

    public static void runPrimeNumber(int number) {
        getSmallerPrimeNrThan(number);
        System.out.println();
    }
}
